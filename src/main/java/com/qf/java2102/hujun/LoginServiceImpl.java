package com.qf.java2102.hujun;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class LoginServiceImpl implements LoginService {

    static final int MAX_ERROR_LOGIN_COUNT = 5;
    static final String ZERO = "0";

    @Autowired
    private RedisTemplate redisTemplate;

    @Resource(name="redisTemplate")
    private ValueOperations<String , String> string;


    /**
     * 判断用户是否被限制登录
     * @param username 用户名
     * @return
     */
    @Override
    public Map<String,Object> checkUserLogin(String username) {
        String lock = RedisKey.getUerLoginLock(username);
        Map<String,Object> map = new HashMap();
        if (string.get(lock)!=null) {
            Long lockTime = redisTemplate.getExpire(lock, TimeUnit.MINUTES);
            map.put("lockTime",lockTime);
            map.put("flag",true);

        } else {
            map.put("flag",false);
        }
        return map;
    }

    /**
     * 检验密码
     * @param username
     * @param password
     * @return
     */
    @Override
    public Map<String,Object> checkPassword(String username,String password) {
        String lock = RedisKey.getUerLoginLock(username);
        String count = RedisKey.getUserLoginErrorCountKey(username);
        String rightPassword = RedisKey.getUserPasswordKey(username);
        Map<String,Object> map = new HashMap();
        //密码正确
        if(string.get(rightPassword)!=null
                &&string.get(rightPassword ).equals(password)){
            redisTemplate.delete(count);
            map.put("flag",true );
            return map;
        } else {//密码错误
            int num = MAX_ERROR_LOGIN_COUNT;
            if (string.get(count)==null){//如果不存在count
                string.set(count,ZERO);
                redisTemplate.expire(count,2,TimeUnit.MINUTES);
                string.increment(count);
                map.put("second",120L);
                map.put("flag",false);
                map.put("count",num-1);
                return map;
            } else {//存在count
                map.put("flag",false);
                //count<4时，自增
                if (Integer.parseInt(string.get(count))<MAX_ERROR_LOGIN_COUNT-1){
                    string.increment(count);
                    Long second = redisTemplate.getExpire(count, TimeUnit.SECONDS);
                    map.put("second",second);
                    map.put("count",num-Integer.parseInt(string.get(count)));
                } else {
                    //count=4时 加lock
                    string.set(lock,"1");
                    redisTemplate.expire(lock,60, TimeUnit.MINUTES);
                    redisTemplate.delete(count);
                    map.put("lock",true);
                }
                return map;
            }
        }
    }


}