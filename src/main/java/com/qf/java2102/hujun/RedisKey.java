package com.qf.java2102.hujun;

public class RedisKey {
    public static String getUserLoginErrorCountKey(String username){
        return "user:"+username+":count";
    }

    public static String getUserPasswordKey(String username){
        return "user:"+username+":password";
    }

    public static String getUerLoginLock(String username){
        return "user:"+username+":lock";
    }
}
