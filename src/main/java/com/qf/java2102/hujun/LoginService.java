package com.qf.java2102.hujun;
import java.util.Map;
public interface LoginService {

    Map<String,Object> checkUserLogin(String username);

    Map<String,Object> checkPassword(String username,String password);
}